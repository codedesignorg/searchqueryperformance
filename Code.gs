/*
	Description: Generate Search Query Performance Reports for all the enabled accounts. Only 'search | beta' campaigns are taken into consideration. MCC level.
    Date: 12/07/2018
    Author: Andrei Ciobanu
*/


function main()
{
   
	//===================================================================================================================
  		// Store the information into arrays
  
	var SPREADSHEET_ID_IMPORT ='1fqwqmP-INyFm1QtWg3D1vjGBYVAYcPkCKZm-4swSru8';
  	var SPREADSHEET_URL_IMPORT ='https://drive.google.com/open?id=1fqwqmP-INyFm1QtWg3D1vjGBYVAYcPkCKZm-4swSru8';
	var SHEET_NAME_IMPORT = 'Dashboard';
	var ss_IMPORT = SpreadsheetApp.openById(SPREADSHEET_ID_IMPORT);
	var sheet_IMPORT = ss_IMPORT.getSheetByName(SHEET_NAME_IMPORT);

	Logger.log(ss_IMPORT.getName());

	var lastRow = sheet_IMPORT.getLastRow();
    var storage_ID=[], storage_Name=[], storage_Enable=[], storage_Limit=[],storage_Time=[];
    var index=0;
  
  
	while (lastRow!=1 )
			{
				var lastColumn = sheet_IMPORT.getLastColumn();
				var lastCell = sheet_IMPORT.getRange(lastRow,lastColumn);
          
          		//Logger.log('Current cell is at %s and has value: "%s".',index,lastCell.getValue());	
          		storage_Time[index]=lastCell.getValue(); lastColumn--;lastCell = sheet_IMPORT.getRange(lastRow,lastColumn);
            	//Logger.log('Current cell is at %s and has value: "%s".',index,lastCell.getValue());
          		storage_Limit[index]=lastCell.getValue(); lastColumn--;lastCell = sheet_IMPORT.getRange(lastRow,lastColumn);
          		//Logger.log('Current cell is at %s and has value: "%s".',index,lastCell.getValue());
          		storage_Enable[index]=lastCell.getValue().toString(); lastColumn--;lastCell = sheet_IMPORT.getRange(lastRow,lastColumn);
          		//Logger.log('Current cell is at %s and has value: "%s".',index,lastCell.getValue());
          		storage_ID[index]=lastCell.getValue().toString(); lastColumn--;lastCell = sheet_IMPORT.getRange(lastRow,lastColumn);
          		Logger.log('Current cell is at %s and has value: "%s".',index,lastCell.getValue());
          		storage_Name[index]=lastCell.getValue().toString(); lastColumn--;
          	
				lastColumn=sheet_IMPORT.getLastColumn();
				lastRow--;
      			index++;
            }
  
	//Logger.log(storage_Enable);
  	Logger.log("\n");
  	Logger.log("Accounts selected: ");
  
  	//======================================================================================================================
	var Valid_indexes = [],Valid_storage_Time = [],Valid_storage_Limit = [],Valid_storage_Enable = [],Valid_storage_ID = [],Valid_storage_Name = [];
    var local = 0;
  
  	for (var i=0;i<=index-1;i++)
  		 {
  			if (storage_Enable[i] === "true")
        		{
                	Valid_indexes.push(i); Valid_storage_Time.push(storage_Time[i]);Valid_storage_Limit.push(storage_Limit[i]);Valid_storage_Enable.push(storage_Enable[i]);Valid_storage_ID.push(storage_ID[i]);Valid_storage_Name.push(storage_Name[i]);
                	Logger.log("Index: %s, time: %s, conversion limit: %s, enable: %s, Id: %s, name: %s",Valid_indexes[local],Valid_storage_Time[local],Valid_storage_Limit[local],Valid_storage_Enable[local],Valid_storage_ID[local],Valid_storage_Name[local]);
              		local++;
        		}
  		 }
      
  	//=========================================================================================================================
	
  	index=Valid_storage_ID.length - 1;
  
  	var accountIterator = MccApp.accounts().withIds(Valid_storage_ID).get();
	var accountCounter = 0;
	var mccAccount = AdWordsApp.currentAccount();  // Save MCC account, to switch back later.
    SpreadsheetApp.setActiveSpreadsheet(ss_IMPORT);
	var sheets_to_clear = SpreadsheetApp.getActiveSpreadsheet().getSheets();
  
    //Logger.log("Number of sheets:%s, first is:%s, second is %s",sheets_to_clear.length,sheets_to_clear[0],sheets_to_clear[1]);
 
  	//=======================================================================================================================
  	// Delete all the tabs at the beginning=========
 	 
  	var i=2;	
  	while (i != sheets_to_clear.length)
  			{
   	 			ss_IMPORT.deleteSheet(sheets_to_clear[i]);
   				i++;
  			} 
  	//===========================================================================================================================
  
  	ss_IMPORT.setActiveSheet(sheet_IMPORT);

	while (accountIterator.hasNext())
		  {                               
         	Logger.log('Has Next');
            
			var account = accountIterator.next();
			MccApp.select(account);  // Save MCC account, to switch back later.
          	ss_IMPORT.insertSheet(account.getName());
         
          //--------------------------------------------------------------------
          	Logger.log("Step 1 : Completed");
            Logger.log("Current account: %s ",account.getName());
		  // Retrieve all beta campaigns.
			var campaignIterator = AdWordsApp.campaigns()
							 				 .withCondition("Name CONTAINS_IGNORE_CASE 'beta'")
							 				 .withCondition("Name CONTAINS_IGNORE_CASE 'search'")
							 				 .withCondition("Status = ENABLED")
						        			 .get();
          	Logger.log("Step 2: Completed");
          	var Conversions_Threshold = Valid_storage_Limit[index]-0.001;
            
          	Logger.log('Conversions Threshold is: %s',Valid_storage_Limit[index]);
        	Logger.log("Step 3: Completed");
          
          	while (campaignIterator.hasNext())
					{		
						var campaign = campaignIterator.next();
                      
						//Logger.log('Current campaign: %s, in account with ID: %s and Name: %s', campaign.getName(),account.getCustomerId(),account.getName());
						//Logger.log("Storge time: %s, index: %s",Valid_storage_Time[index],index);
            
                		var Time_Threshold;
                      
             			//Logger.log("3");
                      
              			if(Valid_storage_Time[index] == 7)
                    		{Time_Threshold = 'LAST_7_DAYS'}
              			else if (Valid_storage_Time[index] == 14)
                    		{Time_Threshold = 'LAST_14_DAYS'}
              			else if (Valid_storage_Time[index] == 30)
                      		{Time_Threshold = 'LAST_30_DAYS'}
                  		else if (Valid_storage_Time[index] == 1)
                      		{Time_Threshold = 'YESTERDAY'}
                  		else if (Valid_storage_Time[index] == 0)
                      		{Time_Threshold = 'TODAY'}
                    	else if (Valid_storage_Time[index]/10000000 > 1 )
                        	{
                              	var today = new Date();
								var dd = today.getDate();
								var mm = today.getMonth()+1; //January is 0!
								var yyyy = today.getFullYear();

								if	(dd<10)	{ dd = '0'+dd } 
								if	(mm<10) { mm = '0'+mm } 
								today = yyyy+mm+dd; Logger.log(today);
                                var Starting_Date = Valid_storage_Time[index].toString();
                              	var CUSTOM_DATE = Starting_Date + ',' + today; 
                              
                              	Time_Threshold = CUSTOM_DATE
                            }
                      
                  
              			//Logger.log("Storage Time: %s, Time_Threshold: %s", Valid_storage_Time[index],Time_Threshold);
                      
              			var Impressions_Limit =0;
             			var columns = ['CampaignName','Query', 'AdGroupName','Impressions','Clicks','Cost','AllConversionValue','Ctr','AveragePosition','Conversions','ConversionRate','CostPerConversion','ConversionValue','ValuePerConversion','QueryTargetingStatus'];
              			//var BETA_SUFFIX = '| Beta';
              			var BETA_SUFFIX = 'beta';
              			var SEARCH_SUFFIX = 'search';
						var reportName = 'SEARCH_QUERY_PERFORMANCE_REPORT';
                      
                      	if (Valid_storage_Time[index] == "ALL")
                      			{	
                                  	var reportQueryTemplate = 'SELECT %s FROM %s WHERE %s';	                                
                                }
                      	else 	{	
                          			var reportQueryTemplate = 'SELECT %s FROM %s WHERE %s DURING %s';
                        		}
						
                      	var crit=[];
						crit.push("CampaignName CONTAINS '"+BETA_SUFFIX+"'");
                		crit.push("CampaignName CONTAINS '"+SEARCH_SUFFIX+"'");
                		crit.push("Conversions> '"+Conversions_Threshold+"'");
                        crit.push("Impressions > '" +Impressions_Limit+ "'");
                      
                      	if (Valid_storage_Time[index] == "ALL")
                      			{
                                  	var reportQuery = Utilities.formatString(reportQueryTemplate, 
                                           						 			columns.join(','), 
                                           						 			reportName,
                                           						 			crit.join(' AND '));            
                                }
                      	else 	{
									var reportQuery = Utilities.formatString(reportQueryTemplate, 
                                           						 			columns.join(','), 
                                           						 			reportName,
                                           						 			crit.join(' AND '),            
                                           						 			Time_Threshold);
                        		}
                      
                     
              
						var report = AdWordsApp.report(reportQuery);
              
            			Logger.log("Campaign Name: %s, on account: %s",campaign.getName(),account.getName());
                
                		report.exportToSheet(ss_IMPORT.getActiveSheet());
            	  
            		}
            
         	Logger.log("Step 4: Completed");
            
          	var sheet = SpreadsheetApp.getActiveSheet();
            
            if (sheet.getRange('A1').isBlank() !== true && sheet.getRange('A2').isBlank() === true)
            		{
                  		Logger.log(sheet.getRange('A1').isBlank());
                  		sheet.getRange('A2').setValue("There is no result found, according to your search criteria.");
                  		sheet.setTabColor("a30303");
                	}
            
            else if (sheet.getRange('A1').isBlank() === true)
            		{
                  		sheet.getRange('A1').setValue("No such campaign in the account.");
                  		sheet.setTabColor("a30303");
                	}
            
          	else 	{
              
            // -------- Set the value for ROAS
          				sheet.getRange("P1").setValue("ROAS").setFontWeight("bold");
    
          				Logger.log("Step 5: Completed");
        
          				for (var k=2;k<=sheet.getLastRow();k++)
         						{
            						var range1 = sheet.getRange(k,16,1,1);
                    				var range2 = sheet.getRange(k,7,1,1); var values2 = range2.getValues();
                    				var range3 = sheet.getRange(k,6,1,1); var values3 = range3.getValues();
           							
                                  	//Logger.log("6");
                                  
                  					var value = values2[0][0]/values3[0][0];
                    				range1.setValue(value);
          						}
          
    		Logger.log("Step 6: Completed");
   //=================================================================================================
          	// Here is the DESIGN part
           				var sheet = SpreadsheetApp.getActiveSheet();
            
           				var range1 = sheet.getRange('A1:P1'); range1.setHorizontalAlignment("center");
		  				var rule1 = SpreadsheetApp.newConditionalFormatRule()  
  												  .whenCellNotEmpty()
  												  .setBackground("#afb8bc")
  												  .setRanges([range1])
  												  .build();
          
           				var range2 = sheet.getRange('J2:J300'); range2.setHorizontalAlignment("center");
           				var rule2 = SpreadsheetApp.newConditionalFormatRule()  
  												  .whenCellNotEmpty()
  											      .setGradientMaxpoint("#db6969")
            									  .setGradientMinpoint("#e8dada")
  												  .setRanges([range2])
  												  .build();
          
           				var range3 = sheet.getRange('A2:A300'); range3.setHorizontalAlignment("center");
           				var range4 = sheet.getRange('K2:P300'); range4.setHorizontalAlignment("center");
           				var range5 = sheet.getRange('C2:I300'); range5.setHorizontalAlignment("center");
           				var rule3 = SpreadsheetApp.newConditionalFormatRule()  
  												  .whenCellNotEmpty()
  												  .setBackground("#eff1f2")
  												  .setRanges([range3,range4,range5])
  												  .build();
          
          				var range6 = sheet.getRange('B2:B300'); range6.setHorizontalAlignment("center");
		   				var rule6 = SpreadsheetApp.newConditionalFormatRule()  
  												  .whenCellNotEmpty()
  												  .setBackground("#F0CC3C")
  												  .setRanges([range6])
  												  .build();
         				
              			//Logger.log("hello");
              
  		   				var rules = sheet.getConditionalFormatRules();
  	       				rules.push(rule1);rules.push(rule2);
           				rules.push(rule3);rules.push(rule6);
           				sheet.setConditionalFormatRules(rules);
              
          				//Logger.log("buna");
              
           				sheet.sort(10, false);
    	   				sheet.setTabColor("F0CC3C");
           				//sheet.setFrozenRows(1);
          			}
             
          index--;
            
     
        } 
  
  
	//==============================================================================================================================================================
            // Here is the EMAIL part
	var SHEET_NAME_IMPORT_3 = 'Cover Page';
	var sheet_IMPORT_3 = ss_IMPORT.getSheetByName(SHEET_NAME_IMPORT_3);   
    ss_IMPORT.setActiveSheet(sheet_IMPORT_3);
	var sheet_3 = SpreadsheetApp.getActiveSheet(); 
  	var default_email = sheet_3.getRange(6,5,1,1).getValue().toString();
  
  	Logger.log(default_email);
  	var Notify_Me = default_email;
  	MailApp.sendEmail(Notify_Me,'Search Query Performance Report - [Search+Beta Campaigns Only] (MCC)', 
                    "  This is an automatic email generated by the Search Query Performance Report - [Search+Beta Campaigns Only] (MCC) script"+
                    ".Your report is ready, please visit the following link:   "+SPREADSHEET_URL_IMPORT+"\n\n"+'   .Thank you! '); 
  
  	//==============================================================================================================================================================
  
}


